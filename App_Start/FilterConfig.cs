﻿using System.Web;
using System.Web.Mvc;
using WebApiMelhorRota.App_Start;

namespace WebApiMelhorRota
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new CustomApiExceptionHandler());
        }
    }
}
