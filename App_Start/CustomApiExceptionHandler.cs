﻿

using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using WebApiMelhorRota.Models;

namespace WebApiMelhorRota.App_Start
{
    public class CustomApiExceptionHandler : ExceptionHandler
    {
        public override void Handle(ExceptionHandlerContext context)
        {            
            context.Result = new TextPlainErrorResult
            {
                Request = context.ExceptionContext.Request,
                Content = "Desculpe aconteceu algo errado. MSG("+ context.Exception.Message+ ")"                
            };
        }

        private class TextPlainErrorResult : IHttpActionResult
        {
            public HttpRequestMessage Request { get; set; }

            public string Content { get; set; }

            public RetornoModel retorno = new RetornoModel();

            public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
            {
                retorno.Mensagem = Content;
                
                return Task.FromResult(Request.CreateResponse(HttpStatusCode.OK, retorno));
            }
        }
    }
}