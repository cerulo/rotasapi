﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.ModelBinding;
using WebApiMelhorRota.Models;

namespace WebApiMelhorRota.Uteis
{
    public class ValidarJson
    {

        public HttpResponseMessage tratarModelState(HttpRequestMessage httpRequestMessage, ModelStateDictionary ModelState)
        {
            string exception = string.Empty;
            string errors = string.Empty;

            exception = string.Join(" | ", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.Exception).Where(d => d != null));    //mau formatado
            errors = string.Join(" | ", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage));    //falta de campos.

            //testa se exception
            if (exception != "")
            {
                errors = exception;
            }

            


            RetornoModel retorno = new RetornoModel();
            retorno.Mensagem = errors;
            return httpRequestMessage.CreateResponse(HttpStatusCode.OK, retorno);
        }
    }
}