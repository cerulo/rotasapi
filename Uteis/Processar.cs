﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace WebApiMelhorRota.Uteis
{
    public class Processar
    {
        /// <summary>
        /// Escalas em memória do arquivo de carga.
        /// </summary>
        private static List<Rotas> listRotasDoArquivo = new List<Rotas>();
        /// <summary>
        /// Objeto contendo todo plano de viagem possivel para rota desejada pelo cliente, ##OBS## este objeto poderia ser salvo
        /// em json para melhor performance e apenas ser atualizado quando valor/rota deste objeto for atualizado.
        /// </summary>
        private static PlanosDeViagem planoDeViagem = new PlanosDeViagem();

        /// <summary>
        /// Processa todas as rotas posiveis para o viagem desejada.
        /// </summary>
        /// <param name="origemCliente"></param>
        /// <param name="destinoCliente"></param>
        public void ProcessarViagen(string origemCliente, string destinoCliente, string path)
        {
            //
            ProcessarArquivo(path);

            //Objeto que vai conter todas rotas de viagem
            List<RotasDeViagem> listRotasDeViagem = new List<RotasDeViagem>();

            //Carregar objeto com todas as rotas da empresa com origem = origem do cliente.
            foreach (Rotas item in listRotasDoArquivo)
            {
                if (item.Origem == origemCliente)
                {
                    RotasDeViagem obj = new RotasDeViagem();
                    obj.Escalas.Add(item);
                    listRotasDeViagem.Add(obj);
                }

            }

            //variável para controlar se todas as rotas foram processadas ate o destino do cliente o destino que não possui mais escala para outros lugares.
            bool finalizada = false;
            while (!finalizada)
            {
                try
                {
                    //Iniciar processamento das rotas.
                    foreach (RotasDeViagem item in listRotasDeViagem)
                    {
                        //Verificas se rota não esta finalizada e tambem não é ainda o destino do cliente.
                        if (item.Finalizada == false && item.Escalas.Last().Destino != destinoCliente)
                        {
                            //Pega proxima rota possivel.
                            List<Rotas> rotasDestino = (from r in listRotasDoArquivo
                                                        where r.Origem == item.Escalas.Last().Destino
                                                        select r).ToList();

                            //Se tiver mais de 1 quer dizer que esta rota possui mais destinos para processar ou seja esta escala pode ir para N destinos.
                            if (rotasDestino.Count > 1)
                            {
                                //Neste caso criamos um novo objeto para salvar a escala atual, pois esta será copiada para a próximo destino.
                                RotasDeViagem rotasDeViagem = new RotasDeViagem();
                                foreach (Rotas obj in item.Escalas)
                                {
                                    rotasDeViagem.Escalas.Add(obj);
                                }

                                //Processar todos o destinos que esta escala possui.
                                for (int i = 0; i < rotasDestino.Count(); i++)
                                {
                                    //Adiciona no RotasDeViagem atual.
                                    if (i == 0)
                                    {
                                        item.Escalas.Add(rotasDestino[i]);
                                    }
                                    else
                                    {
                                        //recriar objeto de escala ate onde ele encotrou n rotas de destino.
                                        RotasDeViagem rotasDeViagem2 = new RotasDeViagem();
                                        foreach (Rotas obj2 in rotasDeViagem.Escalas)
                                        {
                                            rotasDeViagem2.Escalas.Add(obj2);
                                        }

                                        rotasDeViagem2.Escalas.Add(rotasDestino[i]);
                                        listRotasDeViagem.Add(rotasDeViagem2);
                                    }
                                }

                            }
                            else if (rotasDestino.Count == 0)
                            {
                                //sinalizar que esta roda esta finalizada.
                                item.Finalizada = true;
                                item.Mensagem = "Rota final (" + item.Escalas.Last().Destino + ") não possui novas rotas";
                            }
                            else
                            {
                                //Esta rota ainda ainda pode ter outros destinos.
                                item.Escalas.Add(rotasDestino[0]);
                            }
                        }
                        else

                            //sinalizar que esta roda esta finalizada.
                            item.Finalizada = true;
                    }

                }
                catch (Exception)
                {
                    //Editado objeto do foreach para nova rota encontrada.                    
                }

                //Testar se todas rotas foram processada ate o status final
                List<bool> teste = (from r in listRotasDeViagem
                                    where r.Finalizada == false
                                    select r.Finalizada).ToList();

                if (teste.Count == 0)
                {
                    finalizada = true;
                }
            }

            //carrega origem desejada pelo cliente.
            planoDeViagem.Origem = origemCliente;

            //carrega destino desejada pelo cliente.
            planoDeViagem.Destino = destinoCliente;

            //ira controlar todas a viagens possiveis com origem e destino  selecionado pelo cliente.
            List<RotasDeViagem> viagens = new List<RotasDeViagem>();
            foreach (RotasDeViagem item in listRotasDeViagem)
            {
                item.ValorTotalViagem = item.Escalas.Sum(x => x.Valor);
                viagens.Add(item);
            }

            planoDeViagem.Viagens = viagens;

        }

        /// <summary>
        /// Exibir a rota mais em conta para a viagem desejada.
        /// </summary>
        /// <returns></returns>
        public string MelhorRota()
        {
            //Toda rotas possiveis com origem o destino = cliente.
            List<RotasDeViagem> viagens = planoDeViagem.Viagens;

            //Carregar apenas rotas com destino = ao do cliente.
            List<RotasDeViagem> viagensDestinoCliente = new List<RotasDeViagem>();
            foreach (RotasDeViagem item in viagens)
            {
                foreach (Rotas item2 in item.Escalas)
                {
                    //se igual ao destino do cliente carrega para processar menor valor.
                    if (item2.Destino == planoDeViagem.Destino)
                    {
                        item.Mensagem = ">>>>>>>>>>>>>>> OPACAO DE VIAGEM PARA CLIENTE <<<<<<<<<<<<<<";
                        viagensDestinoCliente.Add(item);
                        break;
                    }
                }
            }

            //Testar se existe ao menos uma rota para o destino do cliente
            if (viagensDestinoCliente.Count() == 0)
            {
                return "Desculpe não possuimos rotas com origem " + planoDeViagem.Origem + " até " + planoDeViagem.Destino + ".";
            }

            //conforme desejado será utilizado a rota mais barada para viagem.
            RotasDeViagem rotaMaisBarata = viagensDestinoCliente.OrderBy(v => v.ValorTotalViagem).First();

            //exibir conforme layout desejado melhor rota de viagem            
            StringBuilder rota = new StringBuilder();
            foreach (var item in rotaMaisBarata.Escalas)
            {
                rota.AppendFormat(" - {0}", item.Destino);
            }

            StringBuilder printFull = new StringBuilder();
            foreach (var item in viagens)
            {
                StringBuilder rotasConhecidas = new StringBuilder();
                foreach (var obj in item.Escalas)
                {
                    rotasConhecidas.AppendFormat(" escala [{0} (${1}) {2}]", obj.Origem, obj.Valor, obj.Destino);
                }
                printFull.AppendLine("Viagens >>> " + rotasConhecidas.ToString() + " Mensagem: " + item.Mensagem);

            }

            return "best route: " + planoDeViagem.Origem + rota + " > $" + rotaMaisBarata.ValorTotalViagem;
        }

        /// <summary>
        /// Processar arquivo de carga de rotas.
        /// </summary>
        /// <param name="path">Path completo do arquivo de rotas que deseja carregar na aplicação</param>
        public void ProcessarArquivo(string path)
        {
            try
            {
                //Zerar variavel.
                listRotasDoArquivo.Clear();

                List<string> lines = System.IO.File.ReadLines(path).ToList();
                foreach (string item in lines)
                {
                    string[] linha = item.Split(',');
                    Rotas rota = new Rotas();
                    rota.Origem = linha[0];
                    rota.Destino = linha[1];
                    rota.Valor = float.Parse(linha[2]);

                    listRotasDoArquivo.Add(rota);
                }               
            }
            catch (Exception ex)
            {
                //FinalizarComErro(ex, "Ocorreu um erro na carga do seu arquivo de rotas, verificar arquivo.");
            }
        }

    }
}
