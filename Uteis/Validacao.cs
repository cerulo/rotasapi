﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApiMelhorRota.Models;

namespace WebApiMelhorRota.Uteis
{
    public class Validacao
    {
        /// <summary>
        /// Objeto de retorno da API
        /// </summary>
        RetornoModel retorno = new RetornoModel();

        /// <summary>
        /// Validação da api
        /// </summary>
        /// <param name="origemCliente">Origem da viagem</param>
        /// <param name="destinoCliente">Destino da viagem</param>
        /// <param name="listaArquivo">Objeto com as rotas do arquivo.</param>        
        /// <returns></returns>
        public RetornoModel ValidarViagem(string origemCliente, string destinoCliente, List<RotasModel> listaArquivo = null)
        {
            //Testar se é para validar com arquivo.
            if (listaArquivo != null && listaArquivo.Count() != 0)
            {
                //validar se Origem existe
                List<RotasModel> rotasOrigem = (from r in listaArquivo
                                                where r.Origem == origemCliente                                               
                                                select r).ToList();
                if (rotasOrigem.Count == 0)
                {
                    retorno.Mensagem = "Não temos rotas de partida com a origem ("+origemCliente+").";
                    return retorno;
                }

                //validar se destino existe
                List<RotasModel> rotasDestino = (from r in listaArquivo
                                                where r.Destino == destinoCliente
                                                 select r).ToList();
                if (rotasOrigem.Count == 0)
                {
                    retorno.Mensagem = "Não temos rotas para o destino (" + origemCliente + ").";
                    return retorno;
                }
            }

            //Validar se cliente incluiu mesma rota de origem e destino.
            if (origemCliente == destinoCliente)
            {
                retorno.Mensagem = "Rota de destino identica rota de origem.";
                return retorno;
            }

            return retorno;
        }

        /// <summary>
        /// Validar cadastro de novas rotas.
        /// </summary>
        /// <param name="json">objeto de cadastro</param>
        /// <param name="listaArquivo">objeto com lista de rotas</param>
        /// <returns></returns>
        public RetornoModel ValidarCadastro(RotasModel json, List<RotasModel> listaArquivo = null)
        {
            //Testar se é para validar com arquivo.
            if (listaArquivo != null && listaArquivo.Count() != 0)
            {                
                //validar se Origem existe
                List<RotasModel> rotasIguais = (from r in listaArquivo
                                                where r.Origem == json.Origem.ToUpper()
                                                where r.Destino == json.Destino.ToUpper()
                                                select r).ToList();

                if (rotasIguais.Count > 0)
                {
                    retorno.Mensagem = "Esta rota já existe (" + json.Origem.ToUpper() + "," + json.Destino.ToUpper() + ")";
                    return retorno;
                }
            }

            //Verificar se valor errado.
            if (float.Parse(json.Valor) <= 0)
            {
                retorno.Mensagem = "Campo (Valor) deve ser > 0.";
                return retorno;
            }

            //Validar se cliente incluiu mesma rota de origem e destino.
            if (json.Origem == json.Destino)
            {
                retorno.Mensagem = "Rotas de origem e destino devem ser diverentes.";
                return retorno;
            }

            return retorno;
        }
    }
}