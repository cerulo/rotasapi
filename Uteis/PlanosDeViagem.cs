﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiMelhorRota.Uteis
{
    public class PlanosDeViagem
    { /// <summary>
      /// Origem escolhida pelo cliente.
      /// </summary>
        public string Origem { get; set; }

        /// <summary>
        /// Destino desejado pelo cliente.
        /// </summary>
        public string Destino { get; set; }

        /// <summary>
        /// Viagens disponiveis da empresa para o cliente escolher qual ira adquirir.
        /// </summary>
        public List<RotasDeViagem> Viagens { get; set; }

        public PlanosDeViagem()
        {
            Viagens = new List<RotasDeViagem>();
        }
    }
}