﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WebApiMelhorRota.Models;
using WebApiMelhorRota.Service;
using WebApiMelhorRota.Uteis;

namespace WebApiMelhorRota.Controllers
{
    public class RotasController : ApiController
    {
        /// <summary>
        /// End point para cadastro de novas rotas.
        /// </summary>
        /// <param name="jsonBody">objeto rota</param>
        /// <returns></returns>
        [Route("api/rotas")]
        [HttpPost]
        public async Task<HttpResponseMessage> cadastrar([FromBody] RotasModel jsonBody)
        {
            return await Task.Run(() => {
                HttpResponseMessage ResponseMessage = new HttpResponseMessage();
                if (ModelState.IsValid && jsonBody != null)
                {
                    RotasService service = new RotasService();
                    ResponseMessage = service.cadastrar(this.Request, jsonBody);
                }
                else
                {
                    ValidarJson tratar = new ValidarJson();
                    ResponseMessage = tratar.tratarModelState(this.Request, ModelState);
                }
                return ResponseMessage;
            });
        }

        /// <summary>
        /// End point para consultar todas a rotas da empresa.
        /// </summary>
        /// <returns></returns>
        [Route("api/rotas")]
        [HttpGet]
        public async Task<HttpResponseMessage> consultar()
        {
            return await Task.Run(() => {
                RotasService service = new RotasService();
                return service.consultar(this.Request);
            });
        }

        /// <summary>
        /// End point para processar uma viagem
        /// </summary>
        /// <param name="jsonBody"></param>
        /// <returns></returns>
        [Route("api/processarviagem")]
        [HttpPost]
        public async Task<HttpResponseMessage> processarViagem([FromBody] ViagemModel jsonBody)
        {
            return await Task.Run(() => {
                HttpResponseMessage ResponseMessage = new HttpResponseMessage();
                if (ModelState.IsValid && jsonBody != null)
                {
                    RotasService service = new RotasService();
                    ResponseMessage = service.processarViagem(this.Request, jsonBody);
                }
                else
                {
                    ValidarJson tratar = new ValidarJson();
                    ResponseMessage = tratar.tratarModelState(this.Request, ModelState);
                }
                return ResponseMessage;
            });
        }
    }
}
