﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApiMelhorRota.Models
{
    public class ViagemModel
    {
        [Required]
        /// <summary>
        /// Origem desta Rota.
        /// </summary>
        public string Origem { get; set; }

        [Required]
        /// <summary>
        /// Destino desta Rota.
        /// </summary>
        public string Destino { get; set; }
    }
}