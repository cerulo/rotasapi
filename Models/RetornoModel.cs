﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiMelhorRota.Models
{
    public class RetornoModel
    {        
        /// <summary>
        /// mensagem de retorno
        /// </summary>
        public string Mensagem { get; set; }
        /// <summary>
        /// objeto de retorno
        /// </summary>
        public List<object> Conteudo;
        /// <summary>
        /// Data processamento da request.
        /// </summary>
        public DateTime Data { get; set; }

        public RetornoModel()
        {
            this.Conteudo = new List<object>();
            this.Mensagem = string.Empty ;
            this.Data = DateTime.Now;
        }
    }
}