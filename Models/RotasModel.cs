﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApiMelhorRota.Models
{
    public class RotasModel
    {
        [Required]
        /// <summary>
        /// Origem desta Rota.
        /// </summary>
        public string Origem { get; set; }

        [Required]
        /// <summary>
        /// Destino desta Rota.
        /// </summary>
        public string Destino { get; set; }

        [Required]
        /// <summary>
        /// Valor desta Rota, utilizado para processar o total da viagem desejada pelo cliente.
        /// </summary>
        public string Valor { get; set; }
    }
}