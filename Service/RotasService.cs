﻿using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.WebPages;
using WebApiMelhorRota.App_Start;
using WebApiMelhorRota.Models;
using WebApiMelhorRota.Uteis;

namespace WebApiMelhorRota.Service
{
    public class RotasService
    {
        /// <summary>
        /// objeto retorno da api.
        /// </summary>
        private RetornoModel retorno = new RetornoModel();
        /// <summary>
        /// objeto com lista de rota do arquivo.
        /// </summary>
        private static List<RotasModel> arquivo = new List<RotasModel>();
        /// <summary>
        /// classe de validação
        /// </summary>
        private Validacao check = new Validacao();
        /// <summary>
        /// Carregar path do arquivo a ser manipulado.
        /// </summary>
        private static string filePath = ConfigurationManager.AppSettings["pathArquivo"];

        /// <summary>
        /// Cadastrar Rota no arquivo
        /// </summary>
        /// <param name="httpRequestMessage"></param>
        /// <param name="json"></param>
        /// <returns></returns>
        public HttpResponseMessage cadastrar(HttpRequestMessage httpRequestMessage, RotasModel json)
        {
            try
            {
                //Carrega arquivo atual.
                ProcessarArquivo(filePath);

                //Validar
                retorno = check.ValidarCadastro(json, arquivo);
                if (!retorno.Mensagem.IsEmpty())
                {
                    return httpRequestMessage.CreateResponse(HttpStatusCode.OK, retorno);
                }

                //Testa se arquivo preenchido para pular linha
                if (arquivo.Count > 0)
                {
                    //pular linha
                    File.AppendAllText(filePath, Environment.NewLine);
                }

                //Cadastrar linha em branco e linha nova com registro.                
                File.AppendAllText(filePath, json.Origem + "," + json.Destino + "," + json.Valor);
                

                retorno.Mensagem = "Cadastrado rota com sucesso.";
                return httpRequestMessage.CreateResponse(HttpStatusCode.OK, retorno);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Erro: {0}", ex.Message), ex);
            }

        }

        /// <summary>
        /// Processar viagem para o cliente.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="jsonBody"></param>
        /// <returns></returns>
        internal HttpResponseMessage processarViagem(HttpRequestMessage httpRequestMessage, ViagemModel jsonBody)
        {
            try
            {
                //Carrega arquivo atual.
                ProcessarArquivo(filePath);

                string origem = jsonBody.Origem.ToUpper();
                string destino = jsonBody.Destino.ToUpper();

                //Validar
                retorno = check.ValidarViagem(origem, destino, arquivo);
                if (!retorno.Mensagem.IsEmpty())
                {   
                    return httpRequestMessage.CreateResponse(HttpStatusCode.OK, retorno);
                }

                Processar processar = new Processar();
                processar.ProcessarViagen(origem, destino, filePath);

                retorno.Conteudo.Add(processar.MelhorRota());
                retorno.Mensagem = "Processamento Finalizado.";
                return httpRequestMessage.CreateResponse(HttpStatusCode.OK, retorno);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Erro: {0}", ex.Message), ex);
            }
        }

        /// <summary>
        /// Consulta todas rotas da empresa.
        /// </summary>
        /// <param name="httpRequestMessage"></param>
        /// <returns></returns>
        internal HttpResponseMessage consultar(HttpRequestMessage httpRequestMessage)
        {
            try
            {
                //Carrega arquivo atual.
                ProcessarArquivo(filePath);

                foreach (RotasModel item in arquivo)
                {
                    retorno.Conteudo.Add(item);
                }
                retorno.Mensagem = "Lista completa com "+arquivo.Count()+" rotas.";
                

                return httpRequestMessage.CreateResponse(HttpStatusCode.OK, retorno);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Erro: {0}", ex.Message), ex);
            }
        }

        /// <summary>
        /// Processar arquivo de carga de rotas.
        /// </summary>
        /// <param name="path">Path completo do arquivo de rotas que deseja carregar na aplicação</param>
        private void ProcessarArquivo(string path)
        {   
            //Zerar variavel.
            arquivo.Clear();

            //Verificar se arquivo existe se não criar.
            if (!File.Exists(path))
            {
                File.CreateText(path).Close();
            }

            List<string> lines = System.IO.File.ReadLines(path).ToList();
            foreach (string item in lines)
            {
                string[] linha = item.Split(',');
                RotasModel rota = new RotasModel();
                rota.Origem = linha[0];
                rota.Destino = linha[1];
                rota.Valor = linha[2];
            
                arquivo.Add(rota);
            }
        }

    }
}